class window.LRSView.views.FavoriteColorsTabView extends LRSView.views.TabView

	title: 'Favourite'

class LRSView.views.FavoriteColorsView extends window.LRSView
	
	initialize: ->
		@colors = []
		@editing = false
		#@contentView = @views.content
		
		if (@hasFavorites())
			@_retrieve()
		else
			@colors = [
				{r: 0, g: 0, b:0, get: () ->
					return @
				}
				{r: 255, g: 255, b:255, get: () ->
					return @
				}
			]
			@_store()
			
		@views.list.options.itemClass = window.LRSView.views.FavoriteColorItemView
		@views.list.setContent(@colors)
			
		super
		#FIX: storage event?!
	
	addColorAction: (color, position = null) => #fix params
		color = app.channelManager.getColor()
		
		for i, colorItemView of @views.list.views
			if colorItemView.object and
				colorItemView.object.get('rgb').r == color.r and
				colorItemView.object.get('rgb').g == color.g and
				colorItemView.object.get('rgb').b == color.b
					return false
		
		colorObject = new LRSObject().initialize()
		colorObject.rgb =
			r: color.r
			g: color.g
			b: color.b
		
		@stopEditing()
			
		@colors.push(colorObject)
		@colorsDidChange()
		@colorDidChange(null, colorObject.rgb) #fix; changed values?
		
		color
		
	colorsDidChange: ->
		@views.list.setContent(@colors)
		@_store()
		@trigger('change')
		
	pickColorAction: (color) =>
		rgb = color.get('rgb')
		@trigger('colorPicked', [rgb.r, rgb.g, rgb.b, @])
		@stopEditing()
			
	removeColorAction: (color) =>
		@colors = _.reject(@colors, (item) -> item == color)
		@colorsDidChange()
		
	colorDidChange: (changed, values) =>
	
		active = false
		for i, colorItemView of @views.list.views
			if colorItemView.object and
				colorItemView.object.get('rgb').r == values.r and
				colorItemView.object.get('rgb').g == values.g and
				colorItemView.object.get('rgb').b == values.b
					colorItemView.addClass('active')
					active = true
			else
				colorItemView.removeClass('active')
				
		@views.list.views.addButton.addClass('active') if !active
		
	enableEditModeAction: () =>
		@startEditing()
		
	startEditing: ->
		if (!@editing)
			@editing = true
			@addClass('editing')
	
	stopEditing: ->
		if (@editing)
			@editing = false
			@removeClass('editing')
			
	disable: ->
		@stopEditing()
		super
		
	hasFavorites: ->
		if localStorage.favoriteColors then true else false
	
	_store: ->
		colors = []
		
		for color in @colors
			value = color.get('rgb')
			colors.push({r: value.r, g: value.g, b: value.b})
		
		localStorage.setItem('favoriteColors', JSON.stringify(colors))
	
	_retrieve: ->
		colors = JSON.parse(localStorage.favoriteColors)
		
		@colors = []
		
		for color in colors
			colorObject = new LRSObject().initialize()
			colorObject.rgb =
				r: color.r
				g: color.g
				b: color.b
			@colors.push(colorObject)
		
class window.LRSView.views.FavoriteColorItemView extends LRSView.views.GeneratedListItemView

	template: 'favoriteColorItem'

	customOutlets:
		rgb:
			set: (el, value) ->
				el.css('background-color', 'rgb('+value.r+','+value.g+','+value.b+')')
			get: (el, value) ->
		