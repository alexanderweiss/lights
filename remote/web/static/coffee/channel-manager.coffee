class window.ChannelManager extends LRSObject
	
	initialize: (count = 1) ->
	
		@channels = {}
	
		i = 0
		
		channelNumbers = []
		
		while i < count
			@channels[i] = new Channel().initialize(i)
			channelNumbers.push(i)
			i++
			
		@currentChannels = [@channels[0]]
		@switchChannels(channelNumbers)
		
		super
		
	switchChannels: (channels) ->
		#if (channel.isObject)
		#	@currentChannel = channel
		#else
		channel.off('change', @_valuesDidChange) for channel in @currentChannels
		
		@currentChannels = []
		@currentChannels.push(@channels[channel]) for channel in channels
		
		changed =
			r: @currentChannels[0].get('r')
			g: @currentChannels[0].get('g')
			b: @currentChannels[0].get('b')
			animation: @currentChannels[0].get('animation')
			
		for channel, i in @currentChannels
			continue if i == 0
			channel.setColor(changed.r, changed.g, changed.b, @)
			channel.setAnimation(changed.animation, @)
			
		channel.on('change', @_valuesDidChange) for channel in @currentChannels
		
		@_valuesDidChange(changed, changed, @currentChannels)
		
	getColor: ->
		@currentChannels[0].getColor()
		
	setColor: (r, g, b, sender) ->
		channel.setColor(r, g, b, sender) for channel in @currentChannels
		
	setAnimation: (animation) ->
		channel.setAnimation(animation) for channel in @currentChannels
		
	_valuesDidChange: (changed, values, channel, sender) =>
		#should we check whether it's an active channel etc.?
		@trigger('change', [changed, values, @currentChannels, sender])
		

class window.Channel extends LRSObject
	
	initialize: (@id = 0, @r = 0, @g = 0, @b = 0, @animation = false) ->
		super
	
	setAnimation: (animation = false) ->
		return if animation == @animation
		
		@_valuesDidChange(
			animation:
				old: @animation
				new: animation
		)
		
		@animation = animation
	
	getColor: -> 
		r: @r
		g: @g
		b: @b
	
	setColor: (r = null, g = null, b = null, sender) ->
		changed = {}
		
		if r != null && r != @r
			changed.r = 
				old: @r
				new: r
			@r = r
			
		if g != null && g != @g
			changed.g = 
				old: @g
				new: g
			@g = g
		
		if b != null && b != @b
			changed.b = 
				old: @b
				new: b
			@b = b
			
		@_valuesDidChange(changed, sender) if _.size(changed) > 0
		@
		
	_valuesDidChange: (changed, sender) ->
		@trigger('change', [changed, {r: @r, g: @g, b: @b, animation: @animation}, @, sender])