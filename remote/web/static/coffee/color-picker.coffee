class LRSView.views.ColorPickerView extends LRSView

	template: 'colorPicker'

	initialize: (@color = {r: 0, g: 0, b: 0}) ->
	
		self = @
	
		#Get DOM elements
		@wheelEl = @el.find('.wheel')
		@pickerEl = @wheelEl.find('.picker')
		@valueSliderEl = @el.find('.value-slider')
		
		#Set graph to nothing
		@graph = {}
		
		#Get first offset and set color to whatever was passed
		@determinePickerSize()
		@determineOffset()
		
		@setColor(@color, true)
		
		@
		
	enable: () ->
		self = @
		#Listen to touch events
		@wheelEl.on('touchstart', @touchDidStart)
		@wheelEl.on('touchmove', @touchDidMove)
		@wheelEl.on('touchend', @touchDidEnd)
		#Update value on slider change
		@valueSliderEl.on('change', (e, d) ->
			self.value = $(@).val()
			self.colorShouldChange()
		)
		
		@enabled = true
		@updateDisplay(true)
		
		@
	
	disable: () ->
		#Stop listening to touch events
		@wheelEl.off('touchstart', @touchDidStart)
		@wheelEl.off('touchmove', @touchDidMove)
		@wheelEl.off('touchend', @touchDidEnd)
		#Stop updating slidervalue
		@valueSliderEl.off('change', @valueShouldChange)
		@enabled = false
		
		@
		
	touchDidStart: (e) =>
	
		#Get offset off wheel relative to page (might have changed)
		@determineOffset()
		
		#Make it look like it's moving
		@pickerEl.addClass('picking')
		
		#Reset touch with touch x and y relative to the wheel
		@touch =
			x: e.touches[0].pageX - @offset.x
			y: e.touches[0].pageY - @offset.y
		
		#Position in the graph has changed
		@graphPositionShouldChange()
		
		return
		
	touchDidMove: (e) =>
		#Don't scroll...
		e.preventDefault()
		
		#@touch.prevX = @touch.x
		#@touch.prevY = @touch.y
		
		#Get touch x and y relative to the wheel
		@touch.x = e.touches[0].pageX - @offset.x
		@touch.y = e.touches[0].pageY - @offset.y
		
		#if (@touch.prevX != @touch.x && @touch.prevY != @touch.y)
		#Position in the graph has changed
		@graphPositionShouldChange()
		
	touchDidEnd: (e) =>
		@pickerEl.removeClass('picking')
		
	graphPositionShouldChange: () ->
		#Change into a graph x and y (where the origin; 0, is in the center of the picker instead of top left)
		@graph.x = @touch.x - @offset.r
		@graph.y = - (@touch.y - @offset.r)
		
		#@graph.angle = Math.asin(@graph.y / Math.sqrt(Math.pow(@graph.x, 2) + Math.pow(@graph.y, 2)))
		
		#Get the angle for these coordinates and adjust for our rotation (it's rotated 90 degrees)
		@graph.angle = Math.atan2(@graph.y, @graph.x)
		#Calculate the radius
		@graph.r = Math.sqrt(Math.pow(@graph.x, 2) + Math.pow(@graph.y, 2))
		
		#Don't go outside circle (ser maximum for the radius)
		@graph.r = Math.min(@offset.r, @graph.r)
		
		#Snap to center (white)
		if (@graph.r/@offset.r <= 0.05)
			@graph.r = 0
		
		#Update color
		@colorShouldChange()
	
	colorShouldChange: () ->
		#Set the color
		@setColor(@graphToHsv(@graph.angle, @graph.r, @value))
		
	getColor: () ->
		#Return rgb color value
		@color
		
	setColor: (color, silent = false) =>
		#No color; nothing to do
		if (!color) then return
		
		#Is it hsv (internal) or rgb (external)
		if (_.has(color, 'h'))
			#Set hsv color and calculate rgb color
			hsv_color = color
			rgb_color = @constructor.hsvToRgb(hsv_color.h, hsv_color.s, hsv_color.v)
		else
			#Just set rgb color; hsv comes later
			rgb_color = color
			hsv_color = null
		
		changed = {}
		
		#Set all values on @color that have changed to the new value
		for name, value of rgb_color
			if (@color[name] != value)
				changed[name] = value
				@color[name] = value
		
		#No hsv_color? (which means it's external.
		if (hsv_color == null)
			#Calculate it
			hsv_color = @constructor.rgbToHsv(@color.r, @color.g, @color.b)
			#Get the graph information for them
			graph_values = @hsvToGraph(hsv_color.h, hsv_color.s, hsv_color.v)
			#Set them
			@graph.angle = graph_values.a
			@graph.r = graph_values.r
			#Set the value
			@value = hsv_color.v
			if (@enabled)
				@updateDisplay(true)
		else	
			#Update what we're displaying
			@updateDisplay()
		
		#If we're not silent; fire a change event
		if (_.keys(changed).length && !silent)
			@trigger('colorPicked', [changed.r, changed.g, changed.b, @])
		
	updateDisplay: (updateValue) ->
		if !@enabled then return
		
		#If it should; update the value slider
		if updateValue then @valueSliderEl.val(@value)
		#Get x and y for the picker (from the graph angle and radius)
		pickerX = @offset.r + Math.cos(@graph.angle) * @graph.r
		pickerY = @offset.r + Math.sin(0 - @graph.angle) * @graph.r
		
		#Set CSS values
		@pickerEl.css(
			'-webkit-transform':'translate3d('+ (pickerX - @pickerOffset.x) + 'px, ' + (pickerY - @pickerOffset.y) + 'px, 0)'
			'background-color': 'rgb(' + @color.r + ', ' + @color.g + ', ' + @color.b + ')'
		)
	
	colorDidChange: (changed, values, channel, sender) =>
		return if sender == @
		@setColor(
			r: values.r
			g: values.g
			b: values.b
		, true)
	
	determineOffset: () ->
		#Get the offset values for our wheel.
		position = @wheelEl.offset()
		borderWidth = parseInt(@wheelEl.css('padding'))
		@offset = {x: position.left + borderWidth, y: position.top + borderWidth, r: position.width/2 - borderWidth}
		
	determinePickerSize: () ->
		pickerSize = @pickerEl.offset()
		@pickerOffset = {x: pickerSize.width/2, y: pickerSize.height/2}
		
	offsetDidChange: () ->
		@determinePickerSize()
		@determineOffset()
		@updateDisplay()
	
	#Convert hsv values to graph coordinates
	hsvToGraph: (h, s, v) ->
		return {
			a: h * Math.PI / 180
			r: s * @offset.r / 100
			v: v
		}
	
	#Convert graph coordinates to Hsv values
	graphToHsv: (a, r, v) ->
		#Adjust angle because we've rotated it 90 degrees
		if (a < 0) then a += 2 * Math.PI
		return {
			h: a / Math.PI * 180
			s: r / @offset.r * 100
			v: v
		}
	
	#Convert RGB to HSV
	#Original from http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
	@rgbToHsv: (r, g, b) ->
	
		r = r/255
		g = g/255
		b = b/255
		
		max = Math.max(r, g, b)
		min = Math.min(r, g, b)
		
		v = max
		
		d = max - min
		s = if max == 0 then 0 else d / max
		
		if (max == min)
			h = 0; # achromatic
		else
			switch (max)
				when r
					h = (g - b) / d + (g < b ? 6 : 0)
				when g
					h = (b - r) / d + 2
				when b
					h = (r - g) / d + 4
			h /= 6
		
		return {
			h: h * 360,
			s: s * 100,
			v: v * 100
		}
	
	#Convert HSV to RGB
	#Original from http://snipplr.com/view/14590/hsv-to-rgb/
	@hsvToRgb: (h, s, v) ->
	
		# Make sure our arguments stay in-range
		h = Math.max(0, Math.min(360, h))
		s = Math.max(0, Math.min(100, s))
		v = Math.max(0, Math.min(100, v))
		
		# We accept saturation and value arguments from 0 to 100 because that's
		# how Photoshop represents those values. Internally, however, the
		# saturation and value are calculated from a range of 0 to 1. We make
		# That conversion here.
		s /= 100
		v /= 100
		
		if(s == 0)
			# Achromatic (grey)
			r = g = b = v
			return {
				r: Math.round(r * 255)
				g: Math.round(g * 255)
				b: Math.round(b * 255)
			}
		
		h /= 60 # sector 0 to 5
		i = Math.floor(h)
		f = h - i # factorial part of h
		p = v * (1 - s)
		q = v * (1 - s * f)
		t = v * (1 - s * (1 - f))

		switch (i % 6)
			when 0
				r = v
				g = t
				b = p
			when 1
				r = q
				g = v
				b = p
			when 2
				r = p
				g = v
				b = t
			when 3
				r = p
				g = q
				b = v
			when 4
				r = t
				g = p
				b = v
			when 5
				r = v
				g = p
				b = q
		
		return {
			r: Math.round(r * 255)
			g: Math.round(g * 255)
			b: Math.round(b * 255)
		}