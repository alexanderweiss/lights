class window.ServerConnection extends LRSObject
	
	initialize: (@options = {}) ->
	
		@options = _.extend({minimumPostInterval: 500}, @options)
		
		@parameters = {}
		@postTimeout = null
		
		@
		
	sendCustom: (params) ->
		@parameters = params
		@post()
		
	getInitialData: () ->
		$.getJSON(@options.serverAddress + '/rgb', @processResponse)
	
	#Queue values for red, green etc. to be sent via POST
	queue: (parameters) =>
		for parameter, value of parameters
			#Add parameter and value to the object to be POSTed (or overwrite).
			@parameters[parameter] = value
		
		#Check if there's a timeout running
		if (@postTimeout == null)
			#No; make it
			@postTimeout = setTimeout(@post, @options.minimumPostInterval)
	
	#Send data to Arduino and check if it's recieved it.
	post: () =>
	
		self = @
		
		#Check if any parameters have changed
		if (_.size(@parameters) <= 0)
			#No; wait again
			@postTimeout = null
			#@postTimeout = setTimeout(@post, @options.minimumPostInterval)
			return
		
		#Make snapshot of parameters (which is sent) and empty parameters
		sendParameters = @parameters
		@parameters = {}
		
		#Actually send the parameters
		$.ajax({
			url: @options.serverAddress + '/rgb'
			type: 'POST'
			data: sendParameters
			dataType: 'json'
			timeout: 4000
			success: (data,status) ->
	    	#On success
				#@processResponse(data)
				
				#We're done; a new timeout may be started
				self.postTimeout = null
					
				#Check if there are new values already
				if (_.keys(self.parameters).length > 0)
					#Yes; start timeout right away
					self.postTimeout = setTimeout(self.post, self.options.minimumPostInterval)
			error: (data, errorType, error) ->
			
				self.postTimeout = null
				
				switch errorType
					when 'timeout'
						window.alert('Couldn\'t connect to Lights server')
					else
						window.alert('An unknown error occurred while sending the colours')
		})
	 
	processResponse: (data) =>
		changed = {}
		for param, value of data
			changed[param] = value
			
		@trigger('success', [changed])
		
class window.ServerConnectionEmulator extends ServerConnection

	initialize: ->
		@displayRgb =
			r: 0
			g: 0
			b: 0
			
		super
		
		@el = $('<div class="emulator-display">')
		@el.css('background-color', 'rgb('+@displayRgb.r+', '+@displayRgb.g+', '+@displayRgb.b+')')
		
		$(document.body).append(@el)
		
		@
		
	getInitialData: () ->
		null
		
	post: () =>
		self = @
		
		#Check if any parameters have changed
		if (_.size(@parameters) <= 0)
			@postTimeout = null
			console.log('Nothing changed')
			return
			
		console.log 'sending', @parameters
		
		#Make snapshot of parameters (which is sent) and empty parameters
		sendParameters = @parameters
		@parameters = {}
		
		@displayRgb.r = sendParameters.c0r0.toString().split('_')[0] if (_.has(sendParameters, 'c0r0'))
		@displayRgb.g = sendParameters.c0g0.toString().split('_')[0] if (_.has(sendParameters, 'c0g0'))
		@displayRgb.b = sendParameters.c0b0.toString().split('_')[0] if (_.has(sendParameters, 'c0b0'))
		
		console.log 'displaying', @displayRgb
		
		@el.css('background-color', 'rgb('+@displayRgb.r+', '+@displayRgb.g+', '+@displayRgb.b+')')
		
		if (_.size(@parameters) > 0)
			#Yes; start timeout right away
			@postTimeout = setTimeout(@post, @options.minimumPostInterval)
		else
			@postTimeout = null