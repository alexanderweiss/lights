class window.LightClient extends LRSObject
	
	initialize: (@options = {}) ->
	
		self = @
	
		window.app = @
		
		LRSView.parseTemplates()
		
		@view = new LRSView($('.app'), null, @)
		#@tabManager = new TabManager($('.tab-wrapper'), @view.views.tabBar, @).initialize()
		
		@channelManager = new window.ChannelManager().initialize(@options.channels)
		
		if (window.localStorage.devMode)
			@serverConnection = new ServerConnectionEmulator().initialize({serverAddress: @options.serverAddress})
		else
			@serverConnection = new ServerConnection().initialize({serverAddress: @options.serverAddress})
			
		#@colorPickerTabController = new ColorPickerTabController($('#color-picker-tab')).initialize()
		
		@favoriteColorsView = @view.views.picker.views.back.view
		@favoriteColorsView.on('colorPicked', @colorShouldChange)
		
		@colorPickerView = @view.views.picker.views.front.view
		@colorPickerView.on('colorPicked', @colorShouldChange)
		@colorPickerView.enable()
		
		@channelManager
			.on('change', @favoriteColorsView.colorDidChange)
			.on('change', @colorPickerView.colorDidChange)
			.on('change', @queueChannelChanges)
		
		#@tabManager.addTab(@colorPickerTabController)
		#@tabManager.addTab(@favoriteTabController)
		#@tabManager.setBarTabs([@colorPickerTabController, @view.views.favoriteColorsTab])
		
		#@serverConnection.on('success', @processServerResult)
		window.applicationCache.addEventListener('updateready', @updateApp)
		
		loadingScreenEl = $('.loading-screen')
		
		offset = loadingScreenEl.offset()
		
		loadingScreenEl.css({
			left: offset.left
			top: offset.top
		})
		
		$('.scrollview').css('display', 'block').css('display', '')
		@colorPickerView.offsetDidChange()
		@initScrolling()
		
		loadingScreenEl.animate({
		  scale: 4
		  opacity: 0
		}, 500, 'ease-in', () -> 
			$(this).addClass('hide')
			$('body').removeClass('loading')
		)
		
		@serverConnection.getInitialData()
		
		@
		
	queueChannelChanges: (changed, values, channels) =>
		data = {}
		
		for channel in channels
			if (changed.animation)
				data = changed.animation
				if (changed.animation.new == false)
					data['ca'] = false
					data['c' + channel.id + 'r0'] = values.r
					data['c' + channel.id + 'g0'] = values.g
					data['c' + channel.id + 'b0'] = values.b
			else
				for name, value of changed
					data['c' + channel.id + name + '0'] = value.new
					
		@serverConnection.queue(data)
		
	queueColor: (channels, color) ->
		queueColor = {}
		for name, value of color
			for channel in channels
				queueColor['c' + channel + name] = value
		@serverConnection.queue(queueColor)
		
	processServerResult: (color) =>
		channels = {}
		
		for name, value of color
			channel_name = name.slice(1, -1)
			color_name = name.substr(-1, 1)
			
			if !channels[channel_name] then channels[channel_name] = {}
			
			channels[channel_name][color_name] = value
			
		for name, value of channels
			@colorPickerTabController.setColor(name, value, true)
		
	updateApp: () ->
		window.applicationCache.swapCache()
		
	colorShouldChange: (r, g, b, sender = null) =>
		@channelManager.setColor(r, g, b, sender)
		
	showChannelSwitcherAction: ->
		window.alert('Setting colours for individual channels is not yet supported.')
		
	mainFlipButtonQuickAction: (controller, el) ->
		result = @favoriteColorsView.addColorAction() if (@view.views.picker.get('flipped') == false)
		if (result != false)
			$(el).animate('pulse', {duration: 500})
			
		
	initScrolling: () ->
	
		`if (navigator.standalone){
		    
		    /* if we're in home screen mode:*/
		    window.preventScrolling = true; //flag for when we need to prevent the page bounce:
		    $(document)
		        .on('touchstart','.scrollview-content', function(e){
		            //alternative approach. will turn stale if content is shorter than viewport
		            //window.preventScrolling = (this.offsetHeight <= this.parentNode.offsetHeight);
		            
		            //this approach will keep content elastic even if it is shorter than the viewport:
		            this.style.minHeight = (this.parentNode.offsetHeight+2)+'px';
		            window.preventScrolling=false;
		        })
		        .on('touchmove', function(e){
		            if (window.preventScrolling) e.preventDefault();
		        })
		        .on('touchend', function(){
		            window.preventScrolling = true;
		        });
		    /* */
		
		
		    
		} else {
		    
		    /* Simple version, for mobile pages not running in home screen mode */
		    // here we also need to be able to drag down in the header in order to reach the Address bar
		    $(document)
		        .on('touchstart','.scrollview-content', function(e){
		            //if (this.offsetHeight <= this.parentNode.offsetHeight) 
		            this.style.minHeight = (this.parentNode.offsetHeight+2)+'px';
		        })
		    /* */
		    //prevent rubber-banding on tabbar:
		    $('.tabbar').on('touchmove',function(e){ e.preventDefault(); });
		    
		}
		
		
		// Scroll to top and set viewport dimensions. Useful tips from http://24ways.org/2011/raising-the-bar-on-mobile
		var supportOrientation = typeof window.orientation != 'undefined',
		    getScrollTop = function(){
		        return window.pageYOffset || document.compatMode === 'CSS1Compat' && document.documentElement.scrollTop || document.body.scrollTop || 0;
		    },
		    scrollTop = function(){
		        if (!supportOrientation) return;
		        document.body.style.height = screen.height + 'px';
		        setTimeout(function(){
		            window.scrollTo(0, 1);
		            var top = getScrollTop();
		            window.scrollTo(0, top === 1 ? 0 : 1);
		            document.body.style.height = window.innerHeight + 'px';
		        }, 1);
		    };
		
		if (supportOrientation) window.onorientationchange = scrollTop;
		
		//hide URL bar on page load
		scrollTop();`
		@