class window.TabManager extends LRSObject

	constructor: (@viewEl, @tabBarView, @owner) ->
		@tabBarView.setOwner(@)
		super()
		
	initialize: (@tabControllers = [], @barTabControllers = [], visibleTab) ->
	
		if (visibleTab)
			@currentTab = visibleTab
		else
			@currentTab = @barTabControllers[0]
			
		for controller in @tabControllers
			if (controller != @currentTab)
				controller.hide()
		
		#@tabBarView = new LRSView.views.TabBarView($('.tab-bar')).initialize()
		if (@barTabControllers.length > 0)
			@updateBarView()
		@
		
	#addTab: (controller) ->
		#if (_.has(@tabControllers, controller)) then return false
		#if (!@currentTab) then @currentTab = controller
		
		#@tabControllers.push(controller)
		
	setBarTabs: (@barTabControllers) ->
		@updateBarView()
		@barTabControllers
	
	updateBarView: () ->
			
		@tabBarView.setContent(@barTabControllers)
		
	switchToTab: (controller) ->
		@currentTab.hide()
		@currentTab = controller
		@currentTab.show()
		
	switchToTabAction: (object) ->
		@switchToTab(object)
		

class window.LRSView.views.TabView extends LRSView

	title: null
	icon: null

	constructor: ->
	
		super
		
		if !@view then @view = {el: @el} #backwards compatibility
		
		@hidden = @view.el.hasClass('hide')
		
		@
	
	hide: () ->
	
		if !@hidden then @view.el.addClass('hide')
		@hidden = true
		
	show: () ->
	
		if @hidden then @view.el.removeClass('hide')
		@hidden = false
		
class window.LRSView.views.TabBarView extends LRSView.views.ListView
		
class window.LRSView.views.FlippableTabView extends LRSView.views.TabView

	constructor: ->
	
		super
		
		@frontEl = @view.el.find('.tab-flip-container .front').first()
		@backEl = @view.el.find('.tab-flip-container .back').first()
		@switchBtnEl = @view.el.find('.btn-tab-flip')
		
		@switchBtnEl.on('tap', =>
			@flip()
		)
		
		@flipped = @view.el.hasClass('flipped')
		
		@
		
	flip: () ->
	
		if @flipped
			@flipToFront()
		else
			@flipToBack()
			
	flipToFront: () ->
		@el.removeClass('flipped')
		@switchBtnEl.removeClass('flipped')
		@flipped = false
		
	flipToBack: () ->
		@el.addClass('flipped')
		@switchBtnEl.addClass('flipped')
		@flipped = true