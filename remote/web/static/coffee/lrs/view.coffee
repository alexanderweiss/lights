class window.LRSView extends LRSObject

	@isView = true

	@views = {}
	
	@actionPattern = ///^(.*?):(.*?)\((.*?)\)///

	@parseTemplates: ->
		templateContainer = $('.templates')
		templateContainer.remove()
		templateEls = templateContainer.find('[data-template]')
		@templates = {}
		
		for template in templateEls
			$template = $(template)
			name = $template.attr('data-template')
			$template.removeAttr('data-template')
			@templates[name] = template.outerHTML
			
	constructor: (el = null, @options = null, @owner) ->
		if @template
			@_loadTemplate()
			if (el)
				el.replaceWith(@el)
		else
			@el = el
			
		@_createViews()
		@_createOutlets()
		@_createActions()
		
		super(@options)
		
	initialize: () ->
		@enabled = true
		super
			
	_loadTemplate: ->
		@el = $(LRSView.templates[@template])
		
	_createViews: ->
		viewEls = @el.find('[data-view] ')
		@views = {}
		#console.log @, viewEls
		for view in viewEls
			$view = $(view)
			if !$view.attr('data-view') then continue
			info = $view.attr('data-view').split(':')
			$view.removeAttr('data-view')
			#console.log info[0] || info[1], viewEls, '1'
			if (info.length == 1)
				@views[info[0]] = new LRSView($view, null,  @).initialize()
			else
				@views[info[1]] = new LRSView.views[info[0]+'View']($view, {subTemplate: info[2]}, @).initialize()
				
			if info[0] == 'view' then @view = @views[info[0]]
			else if info[1] == 'view' then @view = @views[info[1]]
			
	_createOutlets: ->
		outletEls = @el.find('[data-outlet]')
		if (@el.attr('data-outlet')) then outletEls.push(@el)
		
		@outlets = {}
		
		for outlet in outletEls
			$outlet = $(outlet)
			#info = $outlet.attr('data-view').split(':')
			#if (info.length == 1)
			#	info.unshift('default')
			#@outlets[info[1]] = {type: info[0], el: $outlet}
			name = $outlet.attr('data-outlet')
			$outlet.removeAttr('data-outlet')
			type = outlet.tagName
			@outlets[name] = {type: type, el: $outlet}
			
			@updateOutletFromDom(name)
			
		if (@customOutlets)
			outlet.type = 'custom' for name, outlet of @customOutlets
			_.extend(@outlets, @customOutlets)
			
		@outlets
		
	_createActions: ->
		els = @el.find('[data-action]')
		if (@el.attr('data-action')) then els.push(@el)
		
		self = @
		
		@actions = {}
		
		for el in els
			$el = $(el)
			actionStrings = $el.attr('data-action').split(';')
			$el.removeAttr('data-action')
			for string in actionStrings
				action = string.match(LRSView.actionPattern)
				action.shift()
				action[2] = action[2].split(',')
				action[2] = [] if action[2][0] == ''

				#if (action[0] == 'tap')
					#action[0] = 'click'
				
				if (!@actions[action[0]])
					@actions[action[0]] = []
					
				$el.on(action[0], (e) -> self.delegateAction(e, @))
				#console.log 'action', action[0], action[1], action[2]
				
				@actions[action[0]].push({el: $el, function: action[1], parameters: action[2]})

		@actions
		
	delegateAction: (e, el = false) =>
		actions = @actions[e.type]
		
		if (!actions) then return
		
		for action in actions
			continue if el != false && action.el[0] != el
			parameters = []
			
			for parameter in action.parameters
				parameters.push(@[parameter])
				
			parameters.push(@)
			parameters.push(el)
			
			@dispatch(action.function + 'Action', parameters)
			
	updateOutletFromDom: (name) ->
		outlet = @outlets[name]
		
		switch outlet.type
			#when 'default'
			when 'custom'
				outlet.get(@el, @[name])
			else
				@[name] = outlet.el.html()
				#Other el types
				
	updateDomFromOutlet: (name) ->
		outlet = @outlets[name]
		
		switch outlet.type
			#when 'default'
			when 'custom'
				outlet.set(@el, @[name])
			else
				outlet.el.html(@[name])
				
	set: (name, value) ->
		@[name] = value
		@updateDomFromOutlet(name)
		value
		
	appendTo: (el) ->
		@el.appendTo(el)
		
	insertBefore: (el) ->
		@el.insertBefore(el)
		
	addClass: (classes) ->
		@el.addClass(classes)
		
	removeClass: (classes) ->
		@el.removeClass(classes)
		
	enable: ->
		@enabled = true
	
	disable: ->
		@enabled = false
	
class window.LRSView.views.ListView extends window.LRSView
	
	initialize: (content) ->
		@permanentContent = @el.children()
		@permanentViews = _.clone(@views)
		@setContent(content) if content
		super()
		
	setContent: (content) ->
		if (!content || !_.isArray(content))
			return
		@content = content
		@views = _.clone(@permanentViews)
		@el.empty()
		@permanentContent.appendTo(@el)
		for object, i in @content
			if (!object.isView)
				if (@options.itemClass)
					view = new @options.itemClass(null, null, @).initialize(object)
				else
					view = new LRSView.views.GeneratedListItemView(@options.subTemplate, null, @).initialize(object)
			else
				view = object
			view.appendTo(@el)
			@views[i] = view
			
class window.LRSView.views.ListItemView extends LRSView

class window.LRSView.views.GeneratedListItemView extends LRSView.views.ListItemView 

	constructor: (template, @options, @owner) ->
		@template = template if template
		
		super(null, options, @owner)

	initialize: (@object) ->
	
		for name, outlet of @outlets
			value = @object.get(name)
			if (value) then @set(name, value)
	
		super(@object)
		
class window.LRSView.views.FlippableView extends LRSView

	constructor: ->
	
		super
		
		@flipped = @el.hasClass('flipped')
		
		if (@flipped)
			@views.back.enable()
			@views.front.disable()
		else
			@views.front.enable()
			@views.back.disable()
		
		@
		
	flip: () ->
	
		if @flipped
			@flipToFront()
		else
			@flipToBack()
			
	flipToFront: () ->
		@el.removeClass('flipped')
		@views.switchButton.removeClass('flipped') if @views.switchButton
		@flipped = false
		@views.front.enable()
		@views.back.disable()
		
	flipToBack: () ->
		@el.addClass('flipped')
		@views.switchButton.addClass('flipped') if @views.switchButton
		@flipped = true
		@views.front.disable()
		@views.back.enable()
		
	viewShouldFlipAction: () ->
		@flip()

class window.LRSView.views.FlippableContentView extends LRSView
	enable: ->
		@view.enable()
	
	disable: ->
		@view.disable()
class window.LRSView.views.FlippableContentFrontView extends LRSView.views.FlippableContentView
class window.LRSView.views.FlippableContentBackView extends LRSView.views.FlippableContentView