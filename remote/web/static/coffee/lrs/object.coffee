class window.LRSObject

	@isObject: true
	
	constructor: (options = {}) ->
	
		if (!@options)
			@options = {}
		_.extend @options, options
		
		@events = {}
		
		@
		
	initialize: () ->
		@
		
	
	on: (event, func) ->
	
		if (!@events[event])
			@events[event] = []
		
		if (_.indexOf(@events[event], func) == -1)
			@events[event].push func
			
		@
	
	off: (event, func) ->
		
		if (!@events[event])
			return
		
		index = _.indexOf(@events[event], func)
		
		@events.splice(index, 1)
		
		@
			
	trigger: (event, attributes) ->
		if(@events[event])
			_.each @events[event], (func) =>
				func.apply(@, attributes)
				
	get: (name) ->
		@[name]
		
	set: (name, value) ->
		@[name] = value
		
	dispatch: (func, parameters = null) ->
		if @[func] && _.isFunction(@[func])
			@[func].apply(@, parameters)
		else if @owner
			@owner.dispatch(func, parameters)
			
	setOwner: (@owner) ->
		@owner