//
//  main.h
//  Arduino Server important stuff
//

struct RGB {
  byte r;
  byte g;
  byte b;
};

// 3*1+1*2 = 5 bytes
struct colorAnimation {
  byte colorStop;
  byte value;
  byte previousValue;
  unsigned int timePassed;
};

// 1*1+2*2 = 5 bytes
struct colorStop {
  bool enabled;
  byte value;
  unsigned int interpolationTime;
  unsigned int startDelay;
};


// 3*5+3*(16)*6+4*1 = 307 bytes (16 color stops)
struct channel {
  colorAnimation r;
  colorAnimation g;
  colorAnimation b;
  colorStop rStops[8];
  colorStop gStops[8];
  colorStop bStops[8];
  byte pinR;
  byte pinG;
  byte pinB;
  boolean repeatAnimation;
};
