//
//  server.info
//  Lights Arduino server
//

#include "SPI.h"
#include "Ethernet.h"
#include "WebServer.h"

#include "main.h"

//Include config file
#include "config.h"
byte mac[] = {
  MAC[0], MAC[1], MAC[2], MAC[3], MAC[4], MAC[5]};
IPAddress ip(IP[0], IP[1], IP[2], IP[3]);

//byte channelCount = CHANNEL_COUNT;
channel channels[CHANNEL_COUNT];

//Keep track of time
unsigned long time = 0;
unsigned int timeDiff = 0;

#define PREFIX "/rgb"
WebServer webServer(PREFIX, 80);

void processRequest(WebServer &server, WebServer::ConnectionType type, char *, bool) {

  if (type == WebServer::POST) {
    bool repeat;
    char name[5], value[16], interpolationTime[5], startDelay[5], channelChar[1], colorStopChar[1];
    int dividerPosition;
    byte valueByte;
    RGB maxStops[CHANNEL_COUNT];
    
    for (byte i = 0; i < CHANNEL_COUNT; i++) {
      maxStops[i].r = 0;
      maxStops[i].g = 0;
      maxStops[i].b = 0;
    }
      
    do
    {
      repeat = server.readPOSTparam(name, 5, value, 16);
      String valueString = String(value);
      String nameString = String(name);
      unsigned int interpolationTimeInt = REQUESTINTERVAL, startDelayInt = 0;
      
      Serial.println("-------------------------------");
      Serial.println(name);
      Serial.println(value);
      
      if (name[1] == 'a') {
        Serial.println(value);
        if (valueString.substring(0, 4) == "true") {
          Serial.println("Starting repeat anim");
          for(byte i=0; i < CHANNEL_COUNT; i++) {
            channels[i].repeatAnimation = true;
          }
        }
        else {
          Serial.println("Stopping repeat anim");
          for(byte i=0; i < CHANNEL_COUNT; i++) {
            channels[i].repeatAnimation = false;
          }
        }
        continue;
      }
      
      nameString.substring(1, 2).toCharArray(channelChar, 2);
      byte channel = atoi(channelChar);
      
      char color = name[2];
      if (color != 'r' && color != 'g' && color != 'b') { continue; }
      
      nameString.substring(3, 4).toCharArray(colorStopChar, 2);
      byte colorStop = atoi(colorStopChar);

      dividerPosition = valueString.indexOf('_');
      if (dividerPosition > 0) {
        valueString.substring(0, dividerPosition).toCharArray(value, 15);
        valueString = valueString.substring(dividerPosition + 1, valueString.length());
        dividerPosition = valueString.indexOf('_');
        if (dividerPosition > 0) {
          valueString.substring(0, dividerPosition).toCharArray(interpolationTime, 6);
          interpolationTimeInt = strtoul(interpolationTime, NULL, 10);
          valueString.substring(dividerPosition + 1, valueString.length()).toCharArray(startDelay, 6);
          startDelayInt = strtoul(startDelay, NULL, 10);
        } 
        else {
          valueString.toCharArray(interpolationTime, 6);
          interpolationTimeInt = strtoul(interpolationTime, NULL, 10);
        }
      }
      valueByte = atoi(value);
      
      Serial.println(String("Channel: ") + channel);
      Serial.println(String("Color: ") + color);
      Serial.println(String("ColorStop: ") + colorStop);
      Serial.println(String("Value: ") + valueByte);
      Serial.println(String("Interpolation: ") + interpolationTimeInt);
      Serial.println(String("Delay: ") + startDelayInt);
      
      switch (color) {
      case 'r':
        maxStops[channel].r = max(colorStop + 1, maxStops[channel].r);
        setColorStop(channels[channel].rStops[colorStop], valueByte, interpolationTimeInt, startDelayInt); 
        break;
      case 'g':
        maxStops[channel].g = max(colorStop + 1, maxStops[channel].g);
        setColorStop(channels[channel].gStops[colorStop], valueByte, interpolationTimeInt, startDelayInt);
        break;
      case 'b':
        maxStops[channel].b = max(colorStop + 1, maxStops[channel].b);
        setColorStop(channels[channel].bStops[colorStop], valueByte, interpolationTimeInt, startDelayInt); 
        break;
      }
    } 
    while (repeat);
    
    for(byte i=0; i < CHANNEL_COUNT; i++) {
      if (maxStops[i].r > 0) {
        channels[i].rStops[maxStops[i].r].enabled = false;
        channels[i].r.colorStop = 0;
        resetColorAnimation(channels[i].r);
      }
      if (maxStops[i].g > 0) {
        channels[i].gStops[maxStops[i].g].enabled = false;
        channels[i].g.colorStop = 0;
        resetColorAnimation(channels[i].g);
      }
      if (maxStops[i].b > 0) {
        channels[i].bStops[maxStops[i].b].enabled = false;
        channels[i].b.colorStop = 0;
         resetColorAnimation(channels[i].b);
      }
    }

  }

  server.httpSuccess("application/json");
  server.print("{");

  for (int channel = 0; channel < CHANNEL_COUNT; channel++) { 
    server.print("\"c" + String(channel) + "r\":");
    server.print(0);
    //server.print(channels[channel].r.target);
    server.print(", \"c" + String(channel) + "g\":");
    server.print(0);
    //server.print(channels[channel].g.target);
    server.print(", \"c" + String(channel) + "b\":");
    server.print(0);
    //server.print(channels[channel].b.target);
    if (channel < CHANNEL_COUNT - 1) server.print(",");
  }
  server.print("}");
  #ifdef DEBUG_MODE
    Serial.println(freeRam());
#endif
  

}


void setup() {

  //Serial debugging and stuff
  Serial.begin(115200);

  //Assign pin numbers
  for (int channel = 0; channel < CHANNEL_COUNT; channel++) {

#ifdef DEBUG_MODE
    Serial.println(String("Assigning channel: ") + channel);
#endif
    //Set pin numbers in channel
    channels[channel].pinR = PINS[channel][0];
    channels[channel].pinG = PINS[channel][1];
    channels[channel].pinB = PINS[channel][2];

    //Set pin mode
    pinMode(PINS[channel][0], OUTPUT);
    pinMode(PINS[channel][1], OUTPUT);
    pinMode(PINS[channel][2], OUTPUT);
  }

  /*channels[0].repeatAnimation = true;

  channels[0].rStops[0].enabled = true;
  channels[0].rStops[0].value = 255;
  channels[0].rStops[0].interpolationTime = 300;
  channels[0].rStops[0].startDelay = 0;

  channels[0].rStops[1].enabled = true;
  channels[0].rStops[1].value = 0;
  channels[0].rStops[1].interpolationTime = 3000;
  channels[0].rStops[1].startDelay = 1000;*/

  //Start ethernet & webserver
  Ethernet.begin(mac, ip);
  webServer.setDefaultCommand(&processRequest);
  webServer.begin();
#ifdef DEBUG_MODE
  Serial.println(String("IP: ") + Ethernet.localIP());
#endif
}

void loop() {

  //Process webserver connection
  webServer.processConnection();

  //Interpolate colors
  interpolateChannels();

  //Get time differences
  timeDiff = (int) millis() - time;
  time = millis();
#ifdef DEBUG_MODE
  if (timeDiff > 3) {
    Serial.println(String("Slow:" ) + timeDiff); 
  }
#endif

}

void setColor (colorStop &color, byte value, int time) {

  //color.previous = color.visible;
  //color.target = value;
  //color.interpolationTime = time;
  //color.timePassed = 0;

}

void setColorStop (colorStop &colorStop, byte value, unsigned int interpolationTime, unsigned int startDelay) {

  colorStop.enabled = true;
  colorStop.value = value;
  colorStop.interpolationTime = interpolationTime;
  colorStop.startDelay = startDelay;

}

void interpolateColor (colorAnimation &color, colorStop stops[], byte pin, bool repeatAnimation) {

  //Check if we need to go to the next stop?
  if (color.value == stops[color.colorStop].value) {

    //Is there a next stop? (no if nr. fifteen or the next is not enabled)
    if (color.colorStop >= 7 || stops[color.colorStop + 1].enabled == false) {
      //No; Can we repeat?
      if (repeatAnimation == true && color.colorStop != 0) {
        //Yes; go to 0 and reset info.
        color.colorStop = 0;
        resetColorAnimation(color);
#ifdef DEBUG_MODE
        Serial.println(String("Color stop: 0 (repeating)" )); 
#endif
      } 
      else {
        //No; nothing to do.
        return; 
      }

    } 
    else {
      //Yes; go to next and reset info.
      color.colorStop++;
      resetColorAnimation(color);
#ifdef DEBUG_MODE
      Serial.println(String("Color stop: ") + color.colorStop); 
#endif

    }

  }
  
  #ifdef DEBUG_MODE
      Serial.println(String("Color stop: ") + color.colorStop); 
    #endif

  colorStop target = stops[color.colorStop];
  
  #ifdef DEBUG_MODE
      Serial.println(String("Time passed: ") + color.timePassed); 
    #endif

  color.timePassed += timeDiff;
  
  #ifdef DEBUG_MODE
      Serial.println(String("Time passed new: ") + color.timePassed); 
    #endif

  //Are we within the timeframe for interpolation?
  if (color.timePassed > target.startDelay + target.interpolationTime || color.timePassed - timeDiff > target.startDelay + target.interpolationTime) {
    //No; just set it to the target
    #ifdef DEBUG_MODE
      Serial.println(String("Alotted time passed")); 
    #endif
    color.value = target.value;
  } 
  else if (color.timePassed >= target.startDelay){
    //Determine the right color
    color.value = (byte) ((color.previousValue - target.value)/2*cos(3.1415/target.interpolationTime*(color.timePassed - target.startDelay)))+((target.value + color.previousValue)/2);
  }

  analogWrite(pin, color.value);

  //if (DEBUG_MODE) Serial.println(color.visible);

}

void resetColorAnimation(colorAnimation &color) {

  color.timePassed = 0;
  color.previousValue = color.value;

}

void interpolateChannel (channel &channel) {
  //Interpolate all colors for the channel
  interpolateColor(channel.r, channel.rStops, channel.pinR, channel.repeatAnimation);
  interpolateColor(channel.g, channel.gStops, channel.pinG, channel.repeatAnimation);
  interpolateColor(channel.b, channel.bStops, channel.pinB, channel.repeatAnimation); 
}

void interpolateChannels () {
  //Iterate all channels and interpolate them
  for (int channel = 0; channel < CHANNEL_COUNT; channel++) { 
    //if (DEBUG_MODE) Serial.println(String("Interpolating channel: ") + channel + String(":") );
    interpolateChannel(channels[channel]);
  }
}


//DEV
int freeRam () {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}



