//
//  config.h
//  Arduino Server config file
//

//Arduino MAC address
const byte MAC[6] = { 0x02, 0xAA, 0xBB, 0xCC, 0x00, 0x22 };
//Arduino IP address (must be the same as set in remote-server)
const byte IP[] = { 192, 168, 1, 1 };

//The interval at which requests are to be expected from the remote (determines interpolation time)
//Should be the same in remote/config/config.js (lower values get quicker response, but are less smooth)
const int REQUESTINTERVAL = 500;

//The amount of channels you want to use (limited by the amount of PWN pins)
const byte CHANNEL_COUNT = 1;
//Decide which pins go for which channel and color - format: {r, g, b}
const byte PINS[CHANNEL_COUNT][3] = {{3,5,6}/*,{7,8,9}*/};

//Enable debug mode (Serial logging)
//#define DEBUG_MODE