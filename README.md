# Lights

Lights is an awesome web app to control RGB LEDs using an Arduino and an Ethernet Shield.

## Quick start

1. Download/clone the repository
2. Install the [Webduino library](https://github.com/sirleech/Webduino)
3. Install the [PWM library](https://code.google.com/p/arduino-pwm-frequency-library/downloads/list)
4. Make a copy of server/config-default.h, name it config.h and change the settings
5. Make a copy of remote/web/config/config-default.js, name it config.js and change the settings
6. Upload server.ino to your Arduino and connect your Arduino to Ethernet + LEDs.
7. Get some web server to serve up remote/web.
8. Control your LEDs!

## Features

- iPhone web app (currently not tested or optimised for other devices than iPhone)
- Uses HTML5 App Cache, so once you've opened it on your phone once, you don't need to have a web server running
- Pick your colour using a colour wheel (and brightness slider)
- Favourite colours, so you can quickly pick that colour you like
- Animation support (the remote app doesn't utilise it right now)

## Development
We're busy adding features and making Lights better. We're mostly developing it for ourselves right now though, so some stuff you find very important might still be missing. You could add it yourself, we'll be happy to have you contribute...

## Arduino Mega timer info
Because lights uses 16-bit colour depth, it is important to know which pins are 16-bit capable. The Arduino Mega has 6 Timers, 4 of which are 16-bit.
Those 4 timers control the following pins:
Timer1: pins 11 and 12
Timer3: pins 2, 3 and 5
Timer4: pins 6, 7 and 8
Timer5: pins 44, 45 and 46
This means that only 3 channels can be used with 16-bit resolution (timer 3, 4 and 5 because each of these timers control 3 pins).

### Planned
- Pre-written lighting animations (and picking them in the remote app)
- Interpolation/delay options when selecting a favourite colour
- Server/app communications on app start to get current colour etc
- Server selection in app

## Contributing
It'd be great if you'd contribute! Just fork and send some awesome pull requests.

## Communications remote + server
The remote communicates with the Arduino server through a POST request.
Information for every colour value is sent in a seperate key-value pair.
The format for these is:

	c[channel number][colour identifier][colour stop number]=[colour value]_[interpolation time]_[start_delay]
	
In which the following values should be set:

- `[channel number]`; the number of the channel, ranging from `0` to `CHANNEL_COUNT - 1`
- `[colour identifier]`; the name of the colour, `r`, `g` or `b`
- `[colour stop number]`; the number of the color stop to be set, ranging from `0` to `7` (if not using animations, only use `0`)
- `[colour value]`; the brightness value for the color specified, ranging from `0` to `255`
- `[interpolation time]`; the time taken to smoothly transition between the previous and target colour, ranging `0` to `65535` milliseconds
- `[start delay]`; the time taken to wait before beginning the transition from the previous colour, ranging `0` to `65535` milliseconds

>`_[start_delay]` may be ommited in which case the color will be applied immediately (it will still be interpolated).

>`_[interpolation time]_[start delay]` may be ommited in which case the `REQUESTINTERVAL` as set in config.h will be used as interpolation time and it will start immediately.

There is one additonal, special, key-value pair to turn repetition of the animation on or off: `ca=[enabled]`, where `enabled` is `true` or `false`. If repetition is turned on, the animation will repeat indefinitely, if it is turned of, it will run from colour stop `0` until the highest specified in the request.

Any value not set in a request will remain unchanged. For example, going from white to red will only require sending values for green and blue.

However, when sending a request, the server will change the animation (or static colour if there's only one stop) to end after the highest colour stop sent in that request. Even when higher colour stops have been set previously they will be ignored (though not discarded).

Note on animations: colour stops for each colour are completely seperate and animate independently. For example (when setting the right values), the red animation might get to colour stop 1 when the green animation is at colour stop six. Furthermore, different colours might have different numbers of stops.

### Examples
- `c0r0=100` will set the red LEDs of channel 0 to 100.
- `c1g0=200_60000` will set the green LEDs of channel 1 to in 60000 milliseconds.
- `c1g0=200_60000_2000` will set the green LEDs of channel 1 to in 60000 milliseconds after waiting for 2000 milliseconds.
- `c0r0=255_2000_1000, c0r1=0_1000_3000` will animate red; waiting 1000 milliseconds, then interpolating to full brightness in 2000 milliseconds, then waiting 3000 milliseconds before interpolating to 0 brightness in 1000 milliseconds. Appending `, ca=true` will repeat this indefinitely.

### Version history

#### 0.1
- Server support for animations
- Favourite colours in app


#### 0.0.1
First version